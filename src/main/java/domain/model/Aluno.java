package domain.model;

import lombok.Builder;
import lombok.Data;
import lombok.With;

@Data
@With
@Builder
public class Aluno {
    private String classe;
    private String nome;
    private String turno;
}
