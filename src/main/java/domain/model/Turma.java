package domain.model;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@Builder
@ToString
public class Turma {
    private String nome;
    private String turno;
    private int quantidadeAlunos;
    private List<Aluno> alunos;
}
