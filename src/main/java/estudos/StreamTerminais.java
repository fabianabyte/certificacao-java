package estudos;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamTerminais {

    public static void main(String a[]){
      List<Integer> lista = Arrays.asList(-9,1,2,3,4,5,6,7,null,3,4,5,66,34,65);

      System.out.println("-----------");

      System.out.println(lista.stream()
                                    .count());
      System.out.println("-----------");

      System.out.println(lista.stream()
                              .filter(e -> e != null)
                              .min(Comparator.naturalOrder())
                                              .get());

      System.out.println("-----------");

      System.out.println(lista.stream()
                              .filter(e -> e != null)
                              .max(Comparator.naturalOrder())
                                              .get());

      System.out.println("-----------");

      System.out.println(lista.stream()
                              .filter(e -> e != null)
                              .collect(Collectors.toList()));

      System.out.println("-----------");

      System.out.println(lista.stream()
                              .filter(e -> e != null)
                              .collect(Collectors.groupingBy(e -> e % 2 == 0)));

      System.out.println("-----------");

      System.out.println(lista.stream()
                              .filter(e -> e != null)
                              .collect(Collectors.groupingBy(e -> e % 4)));

      System.out.println("-----------");

      System.out.println(lista.stream()
                              .map(e -> String.valueOf(e))
                              .collect(Collectors.joining()));

      System.out.println("-----------");

      System.out.println(lista.stream()
                              .map(e -> String.valueOf(e))
                              .collect(Collectors.joining(";")));

      System.out.println("-----------");
    }
}
