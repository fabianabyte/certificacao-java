package estudos;

import java.util.Arrays;
import java.util.List;

public class Reduce {

    public static void main(String a[]){
        List<Integer> lista = Arrays.asList(1,2,3,4,5,6,7,8,9,11,14,16,67,98,56,54,38);

        System.out.println(lista.stream()
                .filter(numero -> numero.intValue() == 39)
                .findAny()
                .orElse(0)
        );

        System.out.println(lista.stream()
                                .reduce((a1 , b1) -> a1 + b1).get());

        System.out.println("-----------");

        System.out.println(lista.stream()
                .reduce(0,(a1 , b1) -> a1 + b1));

        System.out.println("-----------");

        System.out.println(lista.stream()
                .reduce((a1 , b1) -> a1 * b1).get());

        System.out.println("-----------");

        System.out.println(lista.stream()
                .reduce(1,(a1 , b1) -> a1 * b1));

        System.out.println("-----------");

    }
}
