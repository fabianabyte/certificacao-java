package estudos;

import java.time.*;

public class Data {
    public static void main(String[] args) {
        //LocalDate
        System.out.println(LocalDate.now());

        //LocalTime
        System.out.println(LocalTime.now());

        //LocalDateTime
        System.out.println(LocalDateTime.now());

        //Instant
        System.out.println(Instant.now());

        //ZoneDateTime
        System.out.println(ZonedDateTime.now());


    }
}
