package estudos;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class MethodReference {
    public static void main(String a[]){
        List<Integer> lista = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);

        lista.stream()
                .forEach((n) -> System.out.println(n));

        lista.stream()
                .forEach(System.out::println);

        lista.stream()
                .map((n) -> multipliquePorTres(n))
                .forEach((n) -> System.out.println(n));

        lista.stream()
                .map(MethodReference::multipliquePorTres)
                .forEach(System.out::println);

        //construtores
        lista.stream()
                .map((v) -> new BigDecimal(v))
                .forEach(System.out::println);

        lista.stream()
                .map(BigDecimal::new)
                .forEach(System.out::println);

        //passando por várias instâncias
        lista.stream()
                .map((v) -> v.doubleValue())
                .forEach(System.out::println);

        lista.stream()
                .map(Integer::doubleValue)
                .forEach(System.out::println);

        //
        BigDecimal tres = new BigDecimal(3);
        lista.stream()
                .map(BigDecimal::new)
                .map((b) -> tres.multiply(b))
                .forEach(System.out::println);

        lista.stream()
                .map(BigDecimal::new)
                .map(tres::multiply)
                .forEach(System.out::println);
    }

    public static Integer multipliquePorTres(Integer numero){
        return numero * 3;
    }
}