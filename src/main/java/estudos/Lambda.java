package estudos;

public class Lambda {

    public static void main(String a[]){
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("sem lambda!");
            }
        }).run();

        System.out.println("fora thread");

        new Thread(() -> System.out.println("com lambda!")).run();

    }

}
