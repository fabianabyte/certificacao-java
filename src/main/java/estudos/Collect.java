package estudos;

import java.util.*;
import java.util.stream.Collectors;

public class Collect {
    public static void main(String a[]){
        List<Integer> lista = Arrays.asList(1, 2, 3, 4, 5, 6);
        List<Integer> lista2;

        //fornecedor - acumulador - combinação
        lista2 = lista.stream()
                .collect(ArrayList::new, (l, e) -> l.add(e), (l1, l2) -> l1.addAll(l2));

        System.out.println(lista);
        System.out.println(lista2);

        //toList
        lista2 = lista.stream()
                .filter((e) -> e % 2 == 0)
                .collect(Collectors.toList());

        System.out.println(lista2);

        //toSet
        Set<Integer> set = lista.stream()
                .filter((e) -> e % 2 == 0)
                .collect(Collectors.toSet());

        System.out.println(set);

        //toCollect
        Set<Integer> set2 = lista.stream()
                .filter((e) -> e % 2 == 0)
                .collect(Collectors.toCollection(LinkedHashSet::new));

        System.out.println(set2);

        //joining
        System.out.println(lista.stream()
                .map(String::valueOf)
                .collect(Collectors.joining()));

        //averaging
        System.out.println(lista.stream()
                .collect(Collectors.averagingInt(n -> n.intValue())));

        //summing
        System.out.println(lista.stream()
                .collect(Collectors.summingInt(n -> n.intValue())));

        //summarizing
        IntSummaryStatistics stats = lista.stream()
                .collect(Collectors.summarizingInt(n -> n.intValue()));

        System.out.println(stats.getMax());
        System.out.println(stats.getAverage());
        System.out.println(stats.getMin());

        //counting
        System.out.println(lista.stream()
                .collect(Collectors.counting()));

        //max
        lista.stream()
                .collect(Collectors.maxBy(Comparator.naturalOrder()))
                .ifPresent(System.out::println);

        //min
        lista.stream()
                .collect(Collectors.minBy(Comparator.naturalOrder()))
                .ifPresent(System.out::println);


        //groupingBy - agrupa por um critério
        System.out.println(lista.stream()
                .collect(Collectors.groupingBy((n) -> n % 2)));

        //partitionBy - agrupa por booleano
        System.out.println(lista.stream()
                .collect(Collectors.partitioningBy((n) -> n > 3)));

        //toMap - agrupamento que define chave valor
        System.out.println(lista.stream()
                .collect(Collectors.toMap((n) -> n, (n) -> n * 2)));
    }
}
