package estudos;

import java.util.Arrays;
import java.util.List;

public class StreamIntermediarias {

    public static void main(String a[]){
      List<Integer> lista = Arrays.asList(1,2,3,4,5,6,7,null,3,4,5,66,34,65);

      System.out.println("-----------");

      lista.stream()
              .forEach(e -> System.out.println(e));

      System.out.println("-----------");

      lista.stream()
              .skip(2)
              .forEach(e -> System.out.println(e));

      System.out.println("-----------");

      lista.stream()
              .limit(2)
              .forEach(e -> System.out.println(e));

      System.out.println("-----------");

      lista.stream()
              .distinct()//equals e hashcode
              .forEach(e -> System.out.println(e));

      System.out.println("-----------");

      lista.stream()
              .filter(e -> e != null && e % 2 == 0)
              .forEach(e -> System.out.println(e));

      System.out.println("-----------");

      lista.stream()
              .filter(e -> e != null && e % 2 == 0)
              .distinct()
              .forEach(e -> System.out.println(e));

      System.out.println("-----------");

      lista.stream()
              .filter(e -> e != null && e % 2 == 0)
              .map(e -> e = e * 3)
              .forEach(e -> System.out.println(e));

      System.out.println("-----------");
    }
}
