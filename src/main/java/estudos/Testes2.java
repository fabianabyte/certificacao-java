package estudos;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Testes2 {
    public static void main(String[] args) throws IOException {
        //Collections
        List<Integer> lista = Arrays.asList(1,2,3,4,5,6,7,8);
        lista.stream().forEach(System.out::println);

        //Arrays
        Integer[] arrayInt = new Integer[]{1,2,3,4,5};
        Arrays.stream(arrayInt).forEach(System.out::println);

        //Stream.of
        Stream.of("Testes","Stream","of").forEach(System.out::println);

        //IntStreamRange
        IntStream.range(1,4).forEach(System.out::println);
        IntStream.rangeClosed(1,4).forEach(System.out::println);

        //Stream iterate
        Stream.iterate(5,n -> n * 5)
                .limit(15)
                .forEach(System.out::println);

        //BufferedReader
        try(BufferedReader br = new BufferedReader(new FileReader(new File("src/main/java/estudos/numeros.txt")))){
            br.lines().forEach(System.out::println);
        }

        //Files
        Path path = Paths.get("");
        Files.list(path).forEach(System.out::println);

        //Random
        new Random().ints()
                .limit(10)
                .forEach(System.out::println);

        //Pattern
        String s = new String("Testando testando2 ultimo teste");
        Pattern pattern = Pattern.compile(" ");
        pattern.splitAsStream(s)
                .forEach(System.out::println);
    }
}
