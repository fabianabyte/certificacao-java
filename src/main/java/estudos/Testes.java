package estudos;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Testes {
    public static void main(String[] a ){
        DecimalFormat format = new DecimalFormat("___");
        int i = 1;
        System.out.println(format.format(i));
        //Random
        new Random().ints()
                .limit(10)
                .forEach(System.out::println);
   //     testarTodos();
//        testarRepeticoes();
//        testarConversoes();
//        estudandoFlatMap();
    }

    private static void estudandoFlatMap(){
        List<List<Integer>> listOfListofInts = Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(4, 5, 6), Arrays.asList( 7, 8, 9));
        System.out.println(listOfListofInts.stream().flatMap(List::stream).collect(Collectors.toList()));
        System.out.println(listOfListofInts.stream().map(List::stream).collect(Collectors.toList()));
    }

    private static void testarTodos(){
        String textoQueVaiRepetir = "1234";
        Integer repeticoes = 5;
        Integer inicioRepeticoes = 1;
        System.out.println(
                IntStream.rangeClosed(inicioRepeticoes, repeticoes)
                        .mapToObj(String::valueOf)
                        .map(s -> textoQueVaiRepetir)
                        .collect(Collectors.joining())
                        .chars().mapToObj(c -> (char) c)
                        .collect(Collectors.toList())
                        .stream()
                        .map(String::valueOf)
                        .map(BigInteger::new)
                        .collect(Collectors.toList())
                        .stream()
                        .reduce(BigInteger.ZERO, BigInteger::add)
//                        .toString()
//                        .chars()
//                        .mapToObj(c -> (char) c)
//                        .collect(Collectors.toList())
//                        .stream()
//                        .takeWhile(n -> n > 10)

        );
    }

    private static void testarRepeticoes(){
        String repetir = "1234";
        System.out.println(
                IntStream.rangeClosed(1,12)
                        .mapToObj(String::valueOf)
                        .map(s -> repetir)
                        .collect(Collectors.joining())


        );
    }

    private static void testarConversoes(){
        String myString = "123456";
        myString.chars().mapToObj(c -> (char) c).collect(Collectors
                .toList());

        System.out.println(myString.chars().mapToObj(c -> (char) c).collect(Collectors
                .toList()));

        System.out.println(myString.chars().mapToObj(c -> (char) c)
                .collect(Collectors.toList())
                .stream()
                .map(String::valueOf)
                .map(BigInteger::new)
                .collect(Collectors.toList())
        );
    }

}
