package estudos;

import domain.model.Aluno;
import domain.model.Turma;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class GroupingByMap {
    public static void main(String[] args) {
        List<Aluno> alunos = new ArrayList<>();

        alunos.add(Aluno
                .builder()
                .nome("Fabiana")
                .classe("Superior")
                .build());

        alunos.add(Aluno
                .builder()
                .nome("Éden")
                .classe("Superior")
                .build());

        alunos.add(Aluno
                .builder()
                .nome("Alice")
                .classe("Infantil")
                .build());

        alunos.add(Aluno
                .builder()
                .nome("Laura")
                .classe("Infantil")
                .turno("manhã")
                .build());

        alunos.add(Aluno
                .builder()
                .nome("Julia")
                .classe("Infantil")
                .turno("tarde")
                .build());

        coletandoMapa(alunos);
        convertendoMapaEmLista(alunos);

    }

    private static void coletandoMapa(List<Aluno> alunos){
        System.out.println(alunos
                .stream()
                .collect(Collectors.groupingBy(Aluno::getClasse)));


        System.out.println(alunos
                .stream()
                .map(aluno -> aluno.withTurno(aluno.getTurno() == null ? "Sem Turno" : aluno.getTurno()))
                .collect(groupingBy(Aluno::getClasse, groupingBy(Aluno::getTurno))));

        alunos
                .stream()
                .map(aluno -> aluno.withTurno(aluno.getTurno() == null ? "Sem Turno" : aluno.getTurno()))
                .collect(groupingBy(Aluno::getClasse, groupingBy(Aluno::getTurno)))
        .keySet()
        .stream()
        .forEach(System.out::println);
    }
    private static void convertendoMapaEmLista(List<Aluno> alunos){
       Map<String, List<Aluno>> turmas = alunos
                .stream()
                .collect(Collectors.groupingBy(Aluno::getClasse));

       turmas
               .keySet()
               .stream()
               .map(key -> Turma
                           .builder()
                           .alunos(turmas.get(key))
                           .quantidadeAlunos(turmas.get(key).size())
                           .nome(turmas.get(key).stream().findFirst().map(Aluno::getClasse).orElse("Sem Turma"))
                           .turno(turmas.get(key).stream().findFirst().map(Aluno::getTurno).orElse("Sem Turno"))
                           .build())
       .collect(Collectors.toList())
       .forEach(System.out::println);

    }
}
