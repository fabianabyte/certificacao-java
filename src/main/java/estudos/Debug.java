package estudos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Debug {
    public static void main(String a[]){
        List<Integer> lista = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 65, 867, 3453, 234, 45, 774);

        lista.stream()
                .map((n) -> analiseDebug(n))
                .map(String::valueOf)
                .forEach(System.out::println);

        lista.stream()
                .map((integer) -> {
                    integer = integer + 1;
                    integer -= 1;
                    return integer;
                })
                .map(String::valueOf)
                .forEach(System.out::println);

        lista.stream()
                .peek(System.out::println)
                .map(Debug::analiseDebug)
                .peek(System.out::println)
                .map(String::valueOf)
                .peek(System.out::println)
                .collect(Collectors.toList())
                .forEach(System.out::println);


    }

    public static Integer analiseDebug(Integer integer) {
        integer = integer + 1;
        integer -= 1;
        return integer;
    }
}
